package CommonMethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.restassured.response.Response;



public class Utility {




	public static ArrayList<String> ReadExcellData(String SheetName, String TestCase) throws IOException
	{

		ArrayList<String> arrayData = new ArrayList<String>();

		FileInputStream fis = new FileInputStream("Data_Files/Data_Sheet.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook(fis);

		int count = wb.getNumberOfSheets();

		String sheetfound = "False";

		for(int i=0; i<count; i++)
		{
			if(wb.getSheetName(i).equals(SheetName))
			{

				sheetfound = "True";
				XSSFSheet dsheet = wb.getSheetAt(i);

				Iterator<Row>  rows = dsheet.iterator();

				String testcasefound = "false";

				while(rows.hasNext())
				{
					Row datarows = rows.next();

					String tcname = datarows.getCell(0).getStringCellValue();

					if(tcname.equals(TestCase))
					{
						testcasefound = "true";

						Iterator<Cell> cellvalues = datarows.cellIterator();

						while (cellvalues.hasNext()) {
							Cell Cell=cellvalues.next();
							String testdata="";

							//Handle multiple data type method number 1
							/*try {
		    							testdata = Cell.getStringCellValue();
		    							}
		    							catch(IllegalStateException e) {
		    							double inttestData = Cell.getNumericCellValue();
		    							testdata = String.valueOf(inttestData);
		    							}*/

							//Handle multiple data type method number 2

							CellType dataType = Cell.getCellType();
							if(dataType.toString().equals("STRING")) {
								testdata = Cell.getStringCellValue();
							}
							else if(dataType.toString().equals("NUMERIC")) {
								double inttestData = Cell.getNumericCellValue();
								testdata = String.valueOf(inttestData);
							}

							//System.out.println(testdata);
							// Step 4.3 : Add data in array list
							arrayData.add(testdata);
						}


						break;
					}


				}
				if(testcasefound.equals("false"))
				{
					System.out.println(TestCase+"   Test Case not Found in Sheet"+wb.getSheetName(i));  	
				}	
				break;

			}


		}

		if(sheetfound.equals("False"))
		{
			System.out.println(SheetName+"    not Found in Data_Sheet.xlsx");  	

		}	   

		wb.close();
		return arrayData;
	}




	public static File CreateLogDir(String dirName)
	{


		File folder = new File(dirName);

		if(!folder.exists())
		{
			boolean success = folder.mkdir();

			if(success)
				System.out.println("Folder created Successfully");   

			else
				System.out.println("Failed to create Folder . ");   

		}
		else
			System.out.println("Folder already Exist");   


		return folder;

	}


	public static void CreateEvidanceFile(String fileName,File fileLocation,String endpoint,
			String requestBody,String ResHeader,String ResponseBody) throws IOException 
	{


		File newTextFile = new File(fileLocation+"/"+fileName+".txt"); 

		System.out.println("File created with Name    :"+newTextFile.getName());


		FileWriter wd = new FileWriter(newTextFile);

		wd.write("Endpoint is :\n"+endpoint+"\n\n");

		wd.write("Request Body is :\n"+requestBody+"\n\n");

		wd.write("Executed on Date is :\n"+ResHeader+"\n\n");

		wd.write("Response Body  is :\n"+ResponseBody+"\n\n");

		wd.close();                    
	}
	
	
	public static void GetUsersEvidanceFile(String fileName,String fileLocation,String string2) throws IOException 
	{


		File newTextFile = new File(fileLocation+"/"+fileName+".txt"); 

		System.out.println("File created with Name    :"+newTextFile.getName());


		FileWriter wd = new FileWriter(newTextFile);

		wd.write("Users Data is :\n"+string2+"\n\n");
		
		wd.close();                    
	}
}
