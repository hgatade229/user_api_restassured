package Repository;

public class Environment {
	
	   public static String Hostname()
	   {
		   String hostname= "https://dummyjson.com";
		   return hostname;
	   }
	   
	   public static String CreateUserResource()
	   {
		   String resource= "/users/add";
		   return resource;
	   }
	   
	   public static String UpdateUserResource()
	   {
		   String resource= "/users/1";
		   return resource;
	   }
	   
	   
	   public static String GetAllUsersResource()
	   {
		   String resource= "/users";
		   return resource;
	   }
	   
	   public static String DeleteUserResource(String id)
	   {
		   String resource= "/users/"+id;
		   return resource;
	   }
	   
	   public static String Headername()
	   {
		   String headername= "Content-Type";
		   return headername;
	   }
	   
	   public static String Headervalue()
	   {
		   String headervalue= "application/json";
		   return headervalue;
	   }

}
