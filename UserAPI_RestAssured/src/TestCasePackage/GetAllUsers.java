package TestCasePackage;

import java.io.IOException;
import java.util.ArrayList;

import CommonMethod.Api_Trigger;
import CommonMethod.Utility;
import Repository.RequestBody;
import io.restassured.response.Response;

public class GetAllUsers {
	
	    public static void executor() throws IOException
	    {
	    	
	    	String endpoint = RequestBody.Hostname() + RequestBody.GetAllUsersResource();
	    	Response response = Api_Trigger.GetUserApi(RequestBody.Headername(), RequestBody.Headervalue(), endpoint);
	    	
	    	
	    	
	    	Utility.GetUsersEvidanceFile("getAllUsers", "Api_Logs", response.getBody().asPrettyString());
	    	
	        /* int totalUsers = response.getBody().jsonPath().getString("users").length(); 

	    	System.out.println(totalUsers);
	    	//System.out.println(response.getBody().asPrettyString());

    		ArrayList<String> arrayData = new ArrayList<String>();

	    	
	    	for(int i=0;i<10;i++)
	    	{
	    	   
	    		String user = response.getBody().jsonPath().getString("users[i]");
	    		arrayData.add(user);
	    		
	    	}
	    	
	    	System.out.println(arrayData); */

	    	
	    }

}
