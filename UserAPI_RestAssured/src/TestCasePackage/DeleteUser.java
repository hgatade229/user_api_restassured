package TestCasePackage;

import CommonMethod.Api_Trigger;
import Repository.RequestBody;
import io.restassured.response.Response;

public class DeleteUser {
          
	
	public static void executor()
	{
		  
		  
		  String endpoint = RequestBody.Hostname() + RequestBody.DeleteUserResource("1");
		  Response response = Api_Trigger.DeleteUserApi(RequestBody.Headername(), RequestBody.Headervalue(), endpoint);
		  
		  System.out.println(response.getBody().asPrettyString());
		  
	}
}
