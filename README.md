# User_Api_RestAssured



## Projrct Name
User API using Rest Assured
## Description
This project is aimed at ensuring the quality and reliability of User_Api_RestAssured through comprehensive testing methodologies. By implementing rigorous testing processes, we strive to identify and rectify any issues or bugs, ensuring a seamless and efficient user experience.

## Features
Implented tset cases using RestAssured Framework.
Executed CRUD API 

##Technologies Used
Java
RestAssured Framework


#Instructions on how to set up the project for testing.
1. Create Java Project
2. Configure Java Project with Maven 
3. Add dependencies in POM.xml File
  *dependencies
  - RestAssured
  - TestNG
  - ApachePoi



#Sites
Above is implemented by using https://dummyjson.com/docs/users  


## Project status
Project is in perfectly running state
will find more updates in coming days as new functionalities will be added 
